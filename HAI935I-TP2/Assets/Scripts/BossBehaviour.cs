using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BossBehaviour : MonoBehaviour
{
    public Slider healthSlider;
    public GameObject healthSliderObject;
    public TextMeshProUGUI bossTitle;
    public TextMeshProUGUI win;
    public GameObject worldObject;
    private int health = 500;

    void Start()
    {
        healthSlider.value = health;
        win.enabled = false;
        worldObject = GameObject.Find("World");
    }

    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
            healthSliderObject.SetActive(false);
            bossTitle.enabled = false;
            win.enabled = true;
            worldObject.SendMessage("StopTimer");
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && health > 0)
        {
            health--;
            healthSlider.value = health;
        }
    }
}
