using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIBehaviourBis : MonoBehaviour
{
    TMP_Text headText;
    TMP_Text timerText;
    int nbCats = 0;
    //int currentTime = 90;

    IEnumerator TimerTick()
    {
        while (GameVariables.currentTime > 0)
        {
            // attendre une seconde
            yield return new WaitForSeconds(1);
            GameVariables.currentTime--;
            timerText.text = "Time : " + GameVariables.currentTime.ToString();
        }
        // game over
        SceneManager.LoadScene("myNewScene"); // le nom de votre scene (ne pas d�commenter sinon recharge permanente de la sc�ne)
    }

    void Start()
    {
        headText = GameObject.Find("lblCats").GetComponent<TMPro.TMP_Text>();
        timerText = GameObject.Find("lblTime").GetComponent<TMPro.TMP_Text>();
        StartCoroutine(TimerTick());
    }
    
    void Update()
    {

    }

    public void AddHit()
    {
        nbCats++;
        headText.text = "BobHeads: " + nbCats;
    }


    public void StopTimer()
    {
        StopAllCoroutines();
    }
}